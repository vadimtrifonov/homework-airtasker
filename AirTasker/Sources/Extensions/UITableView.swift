import UIKit

extension UITableView {
    
    func dequeueReusableCell<T>(_ type: T.Type) -> T {
        let typeName = String(describing: type)
        guard let cell = dequeueReusableCell(withIdentifier: typeName) as? T else {
            fatalError("Unexpected cell type \(type)")
        }
        return cell
    }
}
