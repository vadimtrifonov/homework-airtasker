import UIKit
import Kingfisher

extension UIImageView {
    
    func setImage(withURL url: URL) {
        kf.setImage(with: url)
    }
}
