import UIKit

extension UIAlertController {
    
    convenience init(error: Error) {
        let message = error is LocalizedError ? error.localizedDescription : "Something went wrong. Try again later.".localized
        
        self.init(title: "Error".localized, message: message, preferredStyle: .alert)
        self.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
    }
}
