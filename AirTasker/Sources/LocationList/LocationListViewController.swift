import UIKit

final class LocationListViewController: UITableViewController {
    
    private enum Identifier {
        static let locationCell = "locationCell"
        static let showLocationSegue = "showLocation"
    }

    var viewModel: LocationListViewModel!
    
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.backgroundView = activityIndicator
        
        title = viewModel.title
        
        activityIndicator.startAnimating()
        refresh()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch (segue.identifier, segue.destination, sender) {
        case (Identifier.showLocationSegue?, let viewController as LocationFeedViewController, let viewModel as LocationFeedViewModel):
                viewController.viewModel = viewModel
        default:
            fatalError("Unxpected segue \(segue.identifier ?? "")")
        }
    }
    
    @IBAction func refresh() {
        viewModel.refresh { [weak self] error in
            guard let _self = self else {
                return
            }
            
            DispatchQueue.main.async {
                _self.activityIndicator.stopAnimating()
                
                if let error = error {
                    return _self.present(UIAlertController(error: error), animated: true) {
                        _self.refreshControl?.endRefreshing()
                    }
                }
                
                _self.refreshControl?.endRefreshing()
                _self.tableView.reloadData()
            }
        }
    }

    // MARK: - UITableViewDataSource

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfLocations()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.locationCell, for: indexPath)
        
        cell.textLabel?.text = viewModel.locationName(at: indexPath)
        
        return cell
    }
        
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let locationFeedViewModel = viewModel.locationFeedViewModel(at: indexPath)
        performSegue(withIdentifier: Identifier.showLocationSegue, sender: locationFeedViewModel)
    }
}
