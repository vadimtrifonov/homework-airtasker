import AirTaskerDomain

final class LocationListViewModel {
    
    var title: String {
        return "Locations".localized
    }
    
    private let locationRepository: LocationRepositoryProtocol
    private let locationFeedViewModelFactory: LocationFeedViewModelFactoryProtocol
    
    private var locations = [Location]()
    
    init(locationRepository: LocationRepositoryProtocol, locationFeedViewModelFactory: LocationFeedViewModelFactoryProtocol) {
        self.locationRepository = locationRepository
        self.locationFeedViewModelFactory = locationFeedViewModelFactory
    }
    
    func refresh(completion: @escaping (Error?) -> Void) {
        locationRepository.loadAllLocations { [weak self] result in
            guard let _self = self else {
                return
            }
            
            do {
                _self.locations = try result.unwrap()
                completion(nil)
            }
            catch {
                completion(error)
            }
        }
    }
    
    func numberOfLocations() -> Int {
        return locations.count
    }
    
    func locationName(at indexPath: IndexPath) -> String {
        return location(at: indexPath).name
    }
    
    func locationFeedViewModel(at indexPath: IndexPath) -> LocationFeedViewModel {
        return locationFeedViewModelFactory.make(location: location(at: indexPath))
    }
    
    private func location(at indexPath: IndexPath) -> Location {
        return locations[indexPath.item]
    }
}
