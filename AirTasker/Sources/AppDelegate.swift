import UIKit
import AirTaskerDomain
import AirTaskerData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        guard
            let navigatonController = window?.rootViewController as? UINavigationController,
            let viewController = navigatonController.viewControllers.first as? LocationListViewController
        else {
            fatalError("Unexpected view controller hierarchy")
        }
        
        viewController.viewModel = makeLocationListViewModel()
        
        return true
    }
    
    func makeLocationListViewModel() -> LocationListViewModel {
        guard let baseURL = URL(string: "https://s3-ap-southeast-2.amazonaws.com/ios-code-test/ios-code-test") else {
            fatalError("URL creation is expected to always succeed")
        }
        
        let urlSession = URLSession(configuration: .default)
        let apiClient = APIClient(baseURL: baseURL, urlSession: urlSession)
        
        let locationRepository = LocationRepository(apiClient: apiClient)
        let profileRepository = ProfileRepository(apiClient: apiClient)
        let taskRepository = TaskRepository(apiClient: apiClient)
        
        let locationFeedInteractor = LocationFeedInteractor(locationRepository: locationRepository, profileRepository: profileRepository, taskRepository: taskRepository)
        let locationFeedViewModelFactory = LocationFeedViewModelFactory(locationFeedInteractor: locationFeedInteractor)
        
        return LocationListViewModel(locationRepository: locationRepository, locationFeedViewModelFactory: locationFeedViewModelFactory)
    }
}
