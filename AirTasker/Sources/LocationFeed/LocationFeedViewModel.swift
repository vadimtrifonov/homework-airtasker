import AirTaskerDomain

final class LocationFeedViewModel {
    
    private struct Section {
        let title: String
        let items: [CellModel]
    }
    
    var title: String {
        return location.name
    }
    
    var locationLatitude: Double {
        return location.latitude
    }
    
    var locationLongitude: Double {
        return location.longitude
    }
    
    private let location: Location
    private let locationFeedInteractor: LocationFeedInteractorProtocol
    private var sections = [Section]()
    
    init(location: Location, locationFeedInteractor: LocationFeedInteractorProtocol) {
        self.location = location
        self.locationFeedInteractor = locationFeedInteractor
    }
    
    func refresh(completion: @escaping (Error?) -> Void) {
        locationFeedInteractor.loadLocationFeed(for: location) { [weak self] result in
            guard let _self = self else {
                return
            }
            
            do {
                let payload = try result.unwrap()
                _self.sections = _self.makeSections(from: payload)
                completion(nil)
            }
            catch {
                completion(error)
            }
        }
    }
    
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func numberOfItems(in section: Int) -> Int {
        return sections[safe: section]?.items.count ?? 0
    }
    
    func cellModel(at indexPath: IndexPath) -> CellModel? {
        guard let cellModel = sections[safe: indexPath.section]?.items[safe: indexPath.item] else {
            return nil
        }
        return cellModel
    }
    
    func titleForHeader(in section: Int) -> String? {
        return sections[safe: section]?.title
    }
    
    private func makeSections(from payload: LocationFeedPayload) -> [Section] {
        return [makeTopRunnersSection(from: payload), makeRecentActivitiesSection(from: payload)].filter({ !$0.items.isEmpty })
    }
    
    private func makeTopRunnersSection(from payload: LocationFeedPayload) -> Section {
        let topRunners = payload.profiles.filter({ payload.locationFeed.workerIDs.contains($0.id) }).sorted(by: >)
        let cellModels = topRunners.map { topRunner in
            return TopRunnerCellModel(profileImageURL: topRunner.avatarMiniURL, name: topRunner.firstName, rating: String(topRunner.rating), description: topRunner.description)
        }
        
        return Section(title: "Top Runners".localized, items: cellModels)
    }
    
    private func makeRecentActivitiesSection(from payload: LocationFeedPayload) -> Section {
        let activities = payload.locationFeed.recentActivities.sorted(by: >)
        let cellModels = activities.flatMap { activity -> ActivityCellModel? in
            guard
                let profile = payload.profiles.first(where: { $0.id == activity.profileID }),
                let task = payload.tasks.first(where: { $0.id == activity.taskID })
            else {
                return nil
            }
            
            let description = activity.makeDescription(profileName: profile.firstName, taskName: task.name)
            let type = activity.event.capitalized
            return ActivityCellModel(profileImageURL: profile.avatarMiniURL, activityDescription: description, activityType: type)
        }

        return Section(title: "Recent Activity".localized , items: cellModels)
    }
}

private extension LocationFeed.Activity {
    
    private static let profileNamePlaceholder = "{profileName}"
    private static let taskNamePlaceholder = "{taskName}"
    
    func makeDescription(profileName: String, taskName: String) -> String {
        return message.replacingOccurrences(of: LocationFeed.Activity.profileNamePlaceholder, with: profileName).replacingOccurrences(of: LocationFeed.Activity.taskNamePlaceholder, with: taskName)
    }
}
