import UIKit

final class ActivityCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var activyDescription: UILabel!
    @IBOutlet weak var activyType: UILabel!
    
    func update(with cellModel: ActivityCellModel) {
        profileImageView.setImage(withURL: cellModel.profileImageURL)
        activyDescription.text = cellModel.activityDescription
        activyType.text = cellModel.activityType
    }
}
