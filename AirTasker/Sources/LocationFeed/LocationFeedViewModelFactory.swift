import AirTaskerDomain

protocol LocationFeedViewModelFactoryProtocol: class {
    
    func make(location: Location) -> LocationFeedViewModel
}

final class LocationFeedViewModelFactory: LocationFeedViewModelFactoryProtocol {
    
    private let locationFeedInteractor: LocationFeedInteractorProtocol
    
    init(locationFeedInteractor: LocationFeedInteractorProtocol) {
        self.locationFeedInteractor = locationFeedInteractor
    }
    
    func make(location: Location) -> LocationFeedViewModel {
        return LocationFeedViewModel(location: location, locationFeedInteractor: locationFeedInteractor)
    }
}
