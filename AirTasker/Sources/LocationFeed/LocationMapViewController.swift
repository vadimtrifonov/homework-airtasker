import UIKit
import MapKit

final class LocationMapViewController: UIViewController {
    
    var locationLatitude: Double!
    var locationLongitude: Double!
    
    @IBOutlet private weak var mapView: MKMapView!
    
    private static let regionDistance: CLLocationDistance = 500

    override func viewDidLoad() {
        super.viewDidLoad()

        let annotation = MKPointAnnotation()
        let coordinate = CLLocationCoordinate2D(latitude: locationLatitude, longitude: locationLongitude)
        annotation.coordinate = coordinate
        
        mapView.addAnnotation(annotation)
        mapView.region = MKCoordinateRegionMakeWithDistance(coordinate, LocationMapViewController.regionDistance, LocationMapViewController.regionDistance)
        mapView.isZoomEnabled = false
        mapView.isScrollEnabled = false
    }
}
