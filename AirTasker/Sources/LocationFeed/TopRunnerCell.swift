import UIKit

final class TopRunnerCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func update(with cellModel: TopRunnerCellModel) {
        profileImageView.setImage(withURL: cellModel.profileImageURL)
        nameLabel.text = cellModel.name
        ratingLabel.text = cellModel.rating
        descriptionLabel.text = cellModel.description
    }
}

