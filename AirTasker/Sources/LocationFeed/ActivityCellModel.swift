import Foundation

struct ActivityCellModel: CellModel {
    
    let profileImageURL: URL
    let activityDescription: String
    let activityType: String
}
