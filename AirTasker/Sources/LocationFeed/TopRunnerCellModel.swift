import Foundation

struct TopRunnerCellModel: CellModel {
    
    let profileImageURL: URL
    let name: String
    let rating: String
    let description: String
}
