import UIKit

final class LocationFeedViewController: UITableViewController {
    
    private enum Identifier {
        static let topRunnerCell = "topRunnerCell"
        static let activityCell = "activityCell"
        static let showLocationMapSegue = "showLocationMap"
    }
    
    var viewModel: LocationFeedViewModel!
    
    @IBOutlet private weak var locationMapContainerView: UIView!
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!
    
    private static let estimatedRowHeight: CGFloat = 101
    private static let locationMapHeight = UIScreen.main.bounds.size.height / 4
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = LocationFeedViewController.estimatedRowHeight
        tableView.tableHeaderView?.frame.size.height = LocationFeedViewController.locationMapHeight
        tableView.backgroundView = activityIndicator
        
        title = viewModel.title
        
        activityIndicator.startAnimating()
        refresh()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if case (Identifier.showLocationMapSegue?, let viewController as LocationMapViewController) = (segue.identifier, segue.destination) {
            viewController.locationLatitude = viewModel.locationLatitude
            viewController.locationLongitude = viewModel.locationLongitude
        }
    }
    
    @IBAction func refresh() {
        viewModel.refresh { [weak self] error in
            guard let _self = self else {
                return
            }
            
            DispatchQueue.main.async {
                _self.activityIndicator.stopAnimating()
                
                if let error = error {
                    return _self.present(UIAlertController(error: error), animated: true)
                }
                
                _self.tableView?.reloadData()
            }
        }
    }
    
    // MARK: - UIScrollView
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            locationMapContainerView.frame.origin.y = scrollView.contentInset.top + scrollView.contentOffset.y
            locationMapContainerView.frame.size.height = LocationFeedViewController.locationMapHeight - scrollView.contentInset.top - scrollView.contentOffset.y
        }
    }
    
    // MARK: - UITableViewDataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems(in: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellModel = viewModel.cellModel(at: indexPath)
        
        switch cellModel {
        case let cellModel as TopRunnerCellModel:
            return dequeueTopRunnerCell(from: tableView, with: cellModel)
        case let cellModel as ActivityCellModel:
            return dequeueActivityCell(from: tableView, with: cellModel)
        default:
            fatalError("Unexpected cellModel \(type(of: cellModel))")
        }
    }
    
    private func dequeueTopRunnerCell(from tableView: UITableView, with cellModel: TopRunnerCellModel) -> TopRunnerCell {
        let cell = tableView.dequeueReusableCell(TopRunnerCell.self)
        cell.update(with: cellModel)
        return cell
    }
    
    private func dequeueActivityCell(from tableView: UITableView, with cellModel: ActivityCellModel) -> ActivityCell {
        let cell = tableView.dequeueReusableCell(ActivityCell.self)
        cell.update(with: cellModel)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.titleForHeader(in: section)
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
