import AirTaskerDomain

extension Location {
    
    enum InitializationError: Error {
        case invalidJSON
    }
    
    init(json: JSONObject) throws {
        guard
            let id = json["id"] as? Int,
            let name = json["display_name"] as? String,
            let latitudeString = json["latitude"] as? String,
            let latitude = Double(latitudeString),
            let longitudeString = json["longitude"] as? String,
            let longitude = Double(longitudeString)
        else {
            throw InitializationError.invalidJSON
        }
        
        self.init(id: id, name: name, latitude: latitude, longitude: longitude)
    }
}

extension Location {
    
    static func makeResourceForAll() -> Resource<[Location]> {
        return Resource(path: "/locations.json") { (json: JSON) in
            guard let json = json as? [JSONObject] else {
                throw Location.InitializationError.invalidJSON
            }
            return try json.map(Location.init)
        }
    }
}
