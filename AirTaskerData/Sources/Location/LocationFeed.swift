import AirTaskerDomain

extension LocationFeed {
    
    enum InitializationError: Error {
        case invalidJSON
    }
    
    init(location: Location, json: JSONObject) throws {
        guard
            let workerCount = json["worker_count"] as? Int,
            let workerIDs = json["worker_ids"] as? [Int],
            let recentActivitiesJSON = json["recent_activity"] as? [JSONObject]
        else {
            throw InitializationError.invalidJSON
        }
        
        let recentActivities = try recentActivitiesJSON.flatMap(Activity.init)
        self.init(location: location, workerCount: workerCount, workerIDs: workerIDs, recentActivities: recentActivities)
    }
}

extension LocationFeed.Activity {
    
    private static let dateFormatter = ISO8601DateFormatter()
    
    init(json: JSONObject) throws {
        guard
            let taskID = json["task_id"] as? Int,
            let profileID = json["profile_id"] as? Int,
            let event = json["event"] as? String,
            let message = json["message"] as? String
        else {
            throw LocationFeed.InitializationError.invalidJSON
        }
        
        var createdAt = Date()
        if
            let rawDate = json["created_at"] as? String,
            let date = LocationFeed.Activity.dateFormatter.date(from: rawDate)
        {
            createdAt = date
        }
        
        self.init(taskID: taskID, profileID: profileID, event: event, message: message, createdAt: createdAt)
    }
}

extension LocationFeed {
    
    static func makeResource(location: Location) -> Resource<LocationFeed> {
        return Resource(path: "/location/\(location.id).json") { (json: JSONObject) in
            return try LocationFeed(location: location, json: json)
        }
    }
}
