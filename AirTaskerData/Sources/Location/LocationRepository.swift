import AirTaskerDomain

public final class LocationRepository: LocationRepositoryProtocol {
    
    private let apiClient: APIClientProtocol
    
    public init(apiClient: APIClientProtocol) {
        self.apiClient = apiClient
    }
    
    public func loadAllLocations(completion: @escaping (Result<[Location]>) -> Void) {
        let resource = Location.makeResourceForAll()
        apiClient.load(resource: resource, completion: completion)
    }
    
    public func loadLocationFeed(for location: Location, completion: @escaping (Result<LocationFeed>) -> Void) {
        let resource = LocationFeed.makeResource(location: location)
        apiClient.load(resource: resource, completion: completion)
    }
}
