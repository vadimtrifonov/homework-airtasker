import AirTaskerDomain

public protocol APIClientProtocol {
    
    var baseURL: URL { get }
    
    func load<Value>(resource: Resource<Value>, completion: @escaping (Result<Value>) -> Void)
    
    func load<Value>(resources: [Resource<Value>], completion: @escaping (Result<[Value]>) -> Void)
}

public final class APIClient: APIClientProtocol {
    
    enum URLSessionError: Error {
        case noData
    }
    
    public let baseURL: URL
    private let urlSession: URLSession
    
    public init(baseURL: URL, urlSession: URLSession) {
        self.baseURL = baseURL
        self.urlSession = urlSession
    }
    
    public func load<Value>(resource: Resource<Value>, completion: @escaping (Result<Value>) -> Void) {
        let task = urlSession.dataTask(with: baseURL.appendingPathComponent(resource.path)) { data, response, error in
            if let error = error {
                return completion(.failure(error))
            }
            
            guard let data = data else {
                return completion(.failure(URLSessionError.noData))
            }
            
            do {
                let value = try resource.parse(data)
                completion(.success(value))
            }
            catch {
                completion(.failure(error))
            }
        }
        
        task.resume()
    }
    
    public func load<Value>(resources: [Resource<Value>], completion: @escaping (Result<[Value]>) -> Void) {
        var values = [Value]()
        var errors = [Error]()
        
        let group = DispatchGroup()
        let workQueue = DispatchQueue(label: "APIClient.workQueue", attributes: .concurrent)
        
        resources.forEach { resource in
            group.enter()
            
            load(resource: resource) { result in
                workQueue.async(flags: .barrier) {
                    do {
                        values.append(try result.unwrap())
                    }
                    catch {
                        errors.append(error)
                    }
                    group.leave()
                }
            }
        }
        
        group.notify(queue: workQueue) {
            if let error = errors.first {
                return completion(.failure(error))
            }
            
            completion(.success(values))
        }
    }
}
