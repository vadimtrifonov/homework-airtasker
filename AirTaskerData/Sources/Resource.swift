import Foundation

typealias JSON = Any
typealias JSONObject = [String: Any]

enum JSONError: Error {
    case parsing
    case notAnObject
}

public struct Resource<Value> {
    let path: String
    let parse: (Data) throws -> Value
}

extension Resource {
    
    init(path: String, parseJSON: @escaping (JSON) throws -> Value) {
        self.init(path: path) { (data: Data) in
            guard let json = try? JSONSerialization.jsonObject(with: data) else {
                throw JSONError.parsing
            }
            return try parseJSON(json)
        }
    }
    
    init(path: String, parseJSONObject: @escaping (JSONObject) throws -> Value) {
        self.init(path: path) { (json: JSON) in
            guard let json = json as? JSONObject else {
                throw JSONError.notAnObject
            }
            return try parseJSONObject(json)
        }
    }
}
