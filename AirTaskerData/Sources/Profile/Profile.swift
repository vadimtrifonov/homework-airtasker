import AirTaskerDomain

extension Profile {
    
    enum InitializationError: Error {
        case invalidJSON
    }
    
    init(json: JSONObject, baseURL: URL) throws {
        guard
            let id = json["id"] as? Int,
            let firstName = json["first_name"] as? String,
            let avatarMiniPath = json["avatar_mini_url"] as? String,
            let rating = json["rating"] as? Float,
            let description = json["description"] as? String
        else {
            throw InitializationError.invalidJSON
        }
        
        let avatarMiniURL = baseURL.appendingPathComponent(avatarMiniPath)
        self.init(id: id, firstName: firstName, avatarMiniURL: avatarMiniURL, rating: rating, description: description)
    }
}

extension Profile {
    
    static func makeResource(id: Int, baseURL: URL) -> Resource<Profile> {
        return Resource(path: "/profile/\(id).json") { (json: JSONObject) in
            return try Profile(json: json, baseURL: baseURL)
        }
    }
}
