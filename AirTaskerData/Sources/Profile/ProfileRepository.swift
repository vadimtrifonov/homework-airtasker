import AirTaskerDomain

public final class ProfileRepository: ProfileRepositoryProtocol {
    
    private let apiClient: APIClientProtocol
    
    public init(apiClient: APIClientProtocol) {
        self.apiClient = apiClient
    }
    
    public func loadProfiles(withIDs profileIDs: Set<Int>, completion: @escaping (Result<[Profile]>) -> Void) {
        let resources = profileIDs.map({ Profile.makeResource(id: $0, baseURL: apiClient.baseURL) })
        apiClient.load(resources: resources, completion: completion)
    }
}
    
