import AirTaskerDomain

public final class TaskRepository: TaskRepositoryProtocol {
    
    private let apiClient: APIClientProtocol
    
    public init(apiClient: APIClientProtocol) {
        self.apiClient = apiClient
    }
    
    public func loadTasks(withIDs taskIDs: Set<Int>, completion: @escaping (Result<[Task]>) -> Void) {
        let resources = taskIDs.map(Task.makeResource)
        apiClient.load(resources: resources, completion: completion)
    }
}
