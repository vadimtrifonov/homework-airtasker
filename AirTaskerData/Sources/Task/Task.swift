import AirTaskerDomain

extension Task {
    
    enum InitializationError: Error {
        case invalidJSON
    }
    
    init(json: JSONObject) throws {
        guard
            let id = json["id"] as? Int,
            let name = json["name"] as? String,
            let rawState = json["state"] as? String,
            let state = State(rawValue: rawState),
            let description = json["description"] as? String,
            let posterID = json["poster_id"] as? Int
        else {
            throw InitializationError.invalidJSON
        }
        
        let workerID = json["worker_id"] as? Int
        self.init(id: id, name: name, state: state, description: description, posterID: posterID, workerID: workerID)
    }
}

extension Task {
        
    static func makeResource(id: Int) -> Resource<Task> {
        return Resource(path: "/task/\(id).json") { (json: JSONObject) in
            return try Task(json: json)
        }
    }
}
