public protocol LocationRepositoryProtocol: class {
    
    func loadAllLocations(completion: @escaping (Result<[Location]>) -> Void)
    
    func loadLocationFeed(for location: Location, completion: @escaping (Result<LocationFeed>) -> Void)
}
