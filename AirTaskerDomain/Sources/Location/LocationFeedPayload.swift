import Foundation

public struct LocationFeedPayload {
    
    public let locationFeed: LocationFeed
    public let profiles: [Profile]
    public let tasks: [Task]
}
