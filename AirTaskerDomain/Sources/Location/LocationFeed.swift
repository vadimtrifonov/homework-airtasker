import Foundation

public struct LocationFeed {
    
    public struct Activity {
        public let taskID: Int
        public let profileID: Int
        public let event: String
        public let message: String
        public let createdAt: Date
        
        public init(taskID: Int, profileID: Int, event: String, message: String, createdAt: Date) {
            self.taskID = taskID
            self.profileID = profileID
            self.event = event
            self.message = message
            self.createdAt = createdAt
        }
    }
    
    public let location: Location
    public let workerCount: Int
    public let workerIDs: [Int]
    public let recentActivities: [Activity]
    
    public init(location: Location, workerCount: Int, workerIDs: [Int], recentActivities: [Activity])  {
        self.location = location
        self.workerCount = workerCount
        self.workerIDs = workerIDs
        self.recentActivities = recentActivities
    }
}

extension LocationFeed.Activity: Comparable {
    
    public static func == (lhs: LocationFeed.Activity, rhs: LocationFeed.Activity) -> Bool {
        return lhs == rhs
    }
    
    public static func < (lhs: LocationFeed.Activity, rhs: LocationFeed.Activity) -> Bool {
        return lhs.createdAt < rhs.createdAt
    }
}
