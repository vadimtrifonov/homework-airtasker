import Foundation

public protocol LocationFeedInteractorProtocol: class {
    
    func loadLocationFeed(for location: Location, completion: @escaping (Result<LocationFeedPayload>) -> Void)
}

public final class LocationFeedInteractor: LocationFeedInteractorProtocol {
    
    private let locationRepository: LocationRepositoryProtocol
    private let profileRepository: ProfileRepositoryProtocol
    private let taskRepository: TaskRepositoryProtocol
    
    public init(locationRepository: LocationRepositoryProtocol, profileRepository: ProfileRepositoryProtocol, taskRepository: TaskRepositoryProtocol) {
        self.locationRepository = locationRepository
        self.profileRepository = profileRepository
        self.taskRepository = taskRepository
    }
    
    public func loadLocationFeed(for location: Location, completion: @escaping (Result<LocationFeedPayload>) -> Void) {
        locationRepository.loadLocationFeed(for: location) { [weak self] result in
            guard let _self = self else {
                return
            }
            
            do {
                let locationFeed = try result.unwrap()
                _self.loadPayload(for: locationFeed, completion: completion)
            }
            catch {
                completion(.failure(error))
            }
        }
    }
    
    private func loadPayload(for locationFeed: LocationFeed, completion: @escaping (Result<LocationFeedPayload>) -> Void) {
        var profiles = [Profile]()
        var tasks = [Task]()
        var errors = [Error]()
        
        let group = DispatchGroup()
        let workQueue = DispatchQueue(label: "LocationFeedInteractor.workQueue", attributes: .concurrent)
        
        group.enter()
        profileRepository.loadProfiles(withIDs: locationFeed.profileIDs) { result in
            workQueue.async(flags: .barrier) {
                do {
                    profiles = try result.unwrap()
                }
                catch {
                    errors.append(error)
                }
                group.leave()
            }
        }
        
        group.enter()
        taskRepository.loadTasks(withIDs: locationFeed.taskIDs) { result in
            workQueue.async(flags: .barrier) {
                do {
                    tasks = try result.unwrap()
                }
                catch {
                    errors.append(error)
                }
                group.leave()
            }
        }
        
        group.notify(queue: workQueue) {
            if let error = errors.first {
                return completion(.failure(error))
            }
            
            let payload = LocationFeedPayload(locationFeed: locationFeed, profiles: profiles, tasks: tasks)
            completion(.success(payload))
        }
    }
}


private extension LocationFeed {
    
    var profileIDs: Set<Int> {
        return Set(workerIDs + recentActivities.map({ $0.profileID }))
    }
    
    var taskIDs: Set<Int> {
        return Set(recentActivities.map({ $0.taskID }))
    }
}
