public protocol TaskRepositoryProtocol: class {
    
    func loadTasks(withIDs ids: Set<Int>, completion: @escaping (Result<[Task]>) -> Void)
}
