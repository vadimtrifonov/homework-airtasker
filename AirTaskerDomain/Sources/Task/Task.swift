import Foundation

public struct Task {
    
    public enum State: String {
        case posted
        case assigned
        case expired
    }
    
    public let id: Int
    public let name: String
    public let state: State
    public let description: String
    public let posterID: Int
    public let workerID: Int?
    
    public init(id: Int, name: String, state: State, description: String, posterID: Int, workerID: Int? = nil) {
        self.id = id
        self.name = name
        self.state = state
        self.description = description
        self.posterID = posterID
        self.workerID = workerID
    }
}
