public protocol ProfileRepositoryProtocol: class {
    
    func loadProfiles(withIDs ids: Set<Int>, completion: @escaping (Result<[Profile]>) -> Void)
}
