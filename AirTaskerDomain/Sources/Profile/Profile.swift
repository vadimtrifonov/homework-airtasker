import Foundation

public struct Profile {
    
    public let id: Int
    public let firstName: String
    public let avatarMiniURL: URL
    public let rating: Float
    public let description: String
    
    public init(id: Int, firstName: String, avatarMiniURL: URL, rating: Float, description: String) {
        self.id = id
        self.firstName = firstName
        self.avatarMiniURL = avatarMiniURL
        self.rating = rating
        self.description = description
    }
}

extension Profile: Comparable {
    
    public static func == (lhs: Profile, rhs: Profile) -> Bool {
        return lhs.id == rhs.id
    }
    
    public static func < (lhs: Profile, rhs: Profile) -> Bool {
        return lhs.rating < rhs.rating
    }
}
